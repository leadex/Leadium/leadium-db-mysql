package org.leadium.db

import org.apache.log4j.Logger
import org.leadium.db.data.DbData
import org.leadium.db.data.LocalData
import org.leadium.db.data.RemoteData
import org.leadium.db.data.SshData
import java.sql.*
import java.sql.ResultSet.CONCUR_UPDATABLE
import java.sql.ResultSet.TYPE_SCROLL_SENSITIVE

/**
 * The class used for executing a SQL statement.
 * then returning a results it produces.
 */
class Jdbc(
    val jdbcDriver: JdbcDriver,
    val url: String,
    val dbData: DbData,
    val autoCommit: Boolean = true
) {

    private val log = Logger.getLogger(this.javaClass.simpleName)

    lateinit var connection: Connection
    lateinit var statement: Statement
    lateinit var savepoint: Savepoint

    fun init() {
        if (this::connection.isInitialized && this::statement.isInitialized) close()
        Class.forName(jdbcDriver.reference).newInstance()
        connection = DriverManager.getConnection("$url?autoReconnect=true&useSSL=false", dbData.user, dbData.password)
        connection.autoCommit = autoCommit
        statement = connection.createStatement()
    }

    /**
     * Executes the SQL statement, which returns a single
     * <code>ResultSet</code> object.
     * <strong>Note:</strong>This method cannot be called on a
     * <code>PreparedStatement</code> or <code>CallableStatement</code>.
     * @param query String an SQL statement to be sent to the database, typically a
     *        static SQL <code>SELECT</code> statement
     * @return a <code>ResultSet</code> object that contains the data produced
     *         by the given query; never <code>null</code>
     */
    fun getResultSet(query: String): ResultSet {
        init()
        log.info(query)
        return connection
            .prepareStatement(query, TYPE_SCROLL_SENSITIVE, CONCUR_UPDATABLE)
            .executeQuery()
    }

    /**
     * Executes the SQL statement, which returns an ArrayList by given column label.
     * <strong>Note:</strong>This method cannot be called on a
     * <code>PreparedStatement</code> or <code>CallableStatement</code>.
     * @param query String an SQL statement to be sent to the database, typically a
     *        static SQL <code>SELECT</code> statement
     * @param columnLabel String
     * @return ArrayList<String>
     */
    fun getColumnArrayList(query: String, columnLabel: String): ArrayList<String> {
        init()
        val target = ArrayList<String>()
        with(getResultSet(query)) {
            while (next()) {
                target.add(getString(columnLabel))
            }
        }
        checkResponseIsNotEmpty(target)
        val response = StringBuilder()
        target.forEach { s: String -> response.append("$s\n") }
        log.info(response)
        return target
    }

    fun checkResponseIsNotEmpty(target: ArrayList<String>) {
        if (target.isEmpty())
            throw Exception("SQL response is empty")
    }

    /**
     * Executes the given SQL statement, which may be an <code>INSERT</code>,
     * <code>UPDATE</code>, or <code>DELETE</code> statement or an
     * SQL statement that returns nothing, such as an SQL DDL statement.
     *<p>
     * <strong>Note:</strong>This method cannot be called on a
     * <code>PreparedStatement</code> or <code>CallableStatement</code>.
     * @param sql an SQL Data Manipulation Language (DML) statement, such as <code>INSERT</code>, <code>UPDATE</code> or
     * <code>DELETE</code>; or an SQL statement that returns nothing,
     * such as a DDL statement.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     */
    fun update(query: String, setSavepoint: Boolean = false): Int {
        init()
        log.info(query)
        if (setSavepoint)
            savepoint = connection.setSavepoint("ROWS_UPDATED")
        val response = statement.executeUpdate(query)
        log.info(response.toString())
        if (!connection.autoCommit)
            connection.commit()
        return response
    }

    /**
     * Undoes all changes made after the given <code>Savepoint</code> object
     * was set.
     * <P>
     * This method should be used only when auto-commit has been disabled.
     *
     * @param savepoint the <code>Savepoint</code> object to roll back to
     * @exception SQLException if a database access error occurs,
     * this method is called while participating in a distributed transaction,
     * this method is called on a closed connection,
     *            the <code>Savepoint</code> object is no longer valid,
     *            or this <code>Connection</code> object is currently in
     *            auto-commit mode
     * @exception SQLFeatureNotSupportedException if the JDBC driver does not support
     * this method
     */
    fun rollback() {
        connection.rollback(savepoint)
    }

    /**
     * Releases this <code>Connection</code> object's database and JDBC resources
     * immediately instead of waiting for them to be automatically released.
     * <P>
     * Calling the method <code>close</code> on a <code>Connection</code>
     * object that is already closed is a no-op.
     * <P>
     * It is <b>strongly recommended</b> that an application explicitly
     * commits or rolls back an active transaction prior to calling the
     * <code>close</code> method.  If the <code>close</code> method is called
     * and there is an active transaction, the results are implementation-defined.
     * <P>
     *
     * Releases this <code>Statement</code> object's database
     * and JDBC resources immediately instead of waiting for
     * this to happen when it is automatically closed.
     * It is generally good practice to release resources as soon as
     * you are finished with them to avoid tying up database
     * resources.
     * <P>
     * Calling the method <code>close</code> on a <code>Statement</code>
     * object that is already closed has no effect.
     * <P>
     * <B>Note:</B>When a <code>Statement</code> object is
     * closed, its current <code>ResultSet</code> object, if one exists, is
     * also closed.
     *
     * @exception SQLException if a database access error occurs
     */
    private fun close() {
        connection.close()
        statement.close()
    }
}