package org.leadium.db

import com.jcraft.jsch.JSch
import com.jcraft.jsch.JSchException
import com.jcraft.jsch.Session
import org.leadium.db.data.LocalData
import org.leadium.db.data.RemoteData
import org.leadium.db.data.SshData

class JSch() {

    private lateinit var session: Session

    @Throws(JSchException::class)
    fun doSshTunnel(
        localData: LocalData,
        remoteData: RemoteData,
        sshData: SshData
    ) {
        val jsch = JSch()
        jsch.addIdentity(sshData.publicKeyPath)
        session = jsch.getSession(sshData.user, sshData.host, sshData.port)
        session.setConfig("PreferredAuthentications", "publickey");
        session.setConfig("StrictHostKeyChecking", "no");
        session.timeout = 8_000
        session.connect()
        session.setPortForwardingL(localData.port, remoteData.host, remoteData.port)
        println("ssh connected: ${session.isConnected}")
    }

    fun disconnect() {
        session.disconnect()
    }
}