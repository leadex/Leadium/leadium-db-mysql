package org.leadium.db.data


data class SshData(
    val user: String,
    val publicKeyPath: String,
    val host: String,
    val port: Int = 22
)

data class LocalData(
    val host: String,
    val port: Int
)

data class RemoteData(
    val host: String,
    val port: Int
)

data class DbData(
    val user: String,
    val password: String
)
